import { format as formatURL } from 'url'
import {
  app,
  BrowserWindow,
  Tray,
  Menu,
  ipcMain,
  globalShortcut,
} from 'electron'
import { indexHtml, logoPng, notifPng } from '../../config/defaults'
import log from './logger'

const logger = log('main')

let mainWindow = null
let mainTray = null

const minimizeTray = false
const COMMAND_CTRL = process.platform === 'darwin' ? 'Cmd' : 'Ctrl'

const shortchuts = (register = true) => {
  if (!register) return globalShortcut.unregisterAll()

  if (
    !globalShortcut.register('CmdOrCtrl+Shift+J', () => {
      // Since this is the only window it has to be the one we're gonna display
      const focusedWindow = BrowserWindow.getFocusedWindow()
      if (mainWindow && focusedWindow) {
        mainWindow.webContents.toggleDevTools()
      }
    })
  ) {
    logger.error(`[hotkey] <${COMMAND_CTRL}+Shift+J> registration failed`)
  }
}

const focusWindow = () => {
  // if the minimize setting is true, it means that we'll hide instead of minimizing
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore()
    mainWindow.focus()
  }
}

const createTray = () => {
  mainTray = new Tray(logoPng)

  mainTray.setToolTip('Anime Downloader')

  const template = [
    {
      label: `v${app.getVersion()}`,
      enabled: false,
    },
    { type: 'separator' },
    {
      label: 'Reload',
      click() {
        if (mainWindow) {
          mainWindow.webContents.reload()
        }
      },
    },
    {
      label: 'Restore',
      click: focusWindow,
    },
    {
      label: 'Quit',
      click() {
        app.quit()
      },
    },
    {
      label: 'Minimize in Tray',
      type: 'checkbox',
      checked: minimizeTray,
      click(ev) {
        if (mainWindow) {
          mainWindow.setSkipTaskbar(ev.checked)
          logger.info(
            `Minimize to tray setting changed to ${
              ev.checked ? 'on' : 'false'
            }.`
          )
        }
      },
    },
  ]

  mainTray.setContextMenu(Menu.buildFromTemplate(template))

  mainTray.on('double-click', focusWindow)
}

const createWindow = () => {
  mainWindow = new BrowserWindow({
    show: false,
    frame: false,
    width: 1152,
    height: 648,
    minWidth: 375,
    minHeight: 667,
    title: 'Anime Downloader',
    icon: logoPng,
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
    shortchuts()
    if (!mainTray) createTray()
  })

  mainWindow.setSkipTaskbar(minimizeTray)

  mainWindow.loadURL(
    formatURL({
      pathname: indexHtml,
      protocol: 'file:',
      slashes: true,
    })
  )
}

ipcMain.on('TRAY_ICON', (event, arg) => {
  if (mainTray) {
    mainTray.setImage(arg ? notifPng : logoPng)
  }
})

if (!app.requestSingleInstanceLock()) {
  app.quit()
} else {
  app.on('single-instance', focusWindow)

  app.on('ready', createWindow)

  app.on('before-quit', () => {
    if (mainWindow) {
      if (mainWindow.webContents.isDevToolsOpened()) {
        mainWindow.webContents.closeDevTools()
      }
    }
  })

  app.on('will-quit', () => {
    if (mainTray && !mainTray.isDestroyed()) {
      mainTray.destroy()
      mainTray = null
    }

    if (mainWindow) {
      mainWindow = null
      shortchuts(false)
    }
  })

  app.on('activate', () => {
    if (!mainWindow) createWindow()
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
  })
}
