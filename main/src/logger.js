import { existsSync, mkdirSync } from 'fs'
import { join as pathJoin } from 'path'
import bunyan from 'bunyan'
import chalk from 'chalk'
import { app } from 'electron'

const userData = app.getPath('userData')

if (!existsSync(userData)) {
  mkdirSync(userData)
}

const dir = pathJoin(userData, './Logs')

if (!existsSync(dir)) {
  mkdirSync(dir)
}

const pad = str => (str.toString().length === 1 ? `0${str}` : str)

const dayMonthYear = (date = new Date()) => {
  const day = pad(date.getDay())
  const month = pad(date.getMonth())
  const year = pad(date.getFullYear())
  return [day, month, year].join('-')
}

const fullyFormattedDate = (date = new Date()) => {
  const dmy = dayMonthYear(date)
  const hour = pad(date.getHours())
  const minute = pad(date.getMinutes())
  const second = pad(date.getSeconds())
  const millisecond = pad(date.getMilliseconds())
  return `${dmy}-${hour}:${minute}:${second}.${millisecond}`
}

const logNames = {
  10: {
    name: 'TRACE',
    color: chalk.gray,
  },
  20: {
    name: 'DEBUG',
    color: chalk.magenta,
  },
  30: {
    name: ' INFO',
    color: chalk.green,
  },
  40: {
    name: ' WARN',
    color: chalk.yellow,
  },
  50: {
    name: 'ERROR',
    color: chalk.red,
  },
  60: {
    name: 'FATAL',
    color: chalk.blueBright,
  },
}

const getErrorMsg = error => (error ? `\n${error.stack || error}` : '')

class RawStream {
  write(record) {
    const level = logNames[record.level] || { name: `LEVEL-${record.level}` }

    // Make sure the message always ends with a fullstop.
    const message = record.msg.endsWith('.') ? record.msg : `${record.msg}`

    process.stdout.write(
      `[${fullyFormattedDate(record.time)}] ${level.color(level.name)} <${
        record.component
      }> - ${message}\n${getErrorMsg(record.err)}`
    )
    return true
  }
}

const logger = bunyan.createLogger({
  name: 'Anime Downloader',
  serializers: bunyan.stdSerializers,
  streams: [
    {
      level: 0,
      stream: new RawStream({ limit: 100 }),
      type: 'raw',
    },
    {
      level: 0,
      path: pathJoin(dir, `./anime-downloader-${dayMonthYear()}.log`),
    },
  ],
})

export default component => logger.child({ component }, true)
