import * as fs from 'fs'
import * as path from 'path'
import { remote } from 'electron'

const cacheDirectory = path.join(remote.app.getPath('userData'), './Cache')
const cacheJSON = path.join(cacheDirectory, './anime.json')
const cacheImages = path.join(cacheDirectory, './images')

// ensure that our cache directory exists
if (!fs.existsSync(cacheDirectory)) fs.mkdirSync(cacheDirectory)

// ensure that our cache images directory exists
if (!fs.existsSync(cacheImages)) fs.mkdirSync(cacheImages)

// ensure that our cache settings exist
if (!fs.existsSync(cacheJSON))
  fs.writeFileSync(cacheJSON, '[]', { encoding: 'utf-8' })

// NOTE: the cache service only holds the animes which aren't ongoing
class CacheService {
  cache = []

  constructor() {
    this.refresh()
  }

  get(anime) {
    if (!this.cache.length) return null
    const query = this.cache.find(({ link }) => anime === link)
    if (query) return query
    return null
  }

  has(anime) {
    return !!this.get(anime)
  }

  delete(anime) {
    if (this.has(anime)) {
      const index = this.cache.findIndex(({ link }) => anime === link)
      this.cache.splice(index, 1)
      this._write()
    }
  }

  add(anime, details) {
    if (!this.has(anime)) {
      for (const key of Object.keys(details)) {
        if (key.startsWith('$')) delete details[key]
      }

      this.cache.push(details)
      this._write()
    }
  }

  _write(data = this.cache) {
    fs.writeFileSync(
      cacheJSON,
      JSON.stringify(
        data,
        (key, value) => {
          return key === '$$hashKey' ? undefined : value
        },
        '\t'
      ),
      {
        encoding: 'utf-8',
      }
    )
  }

  refresh() {
    console.info(`Loading cache configuration from: '${cacheJSON}'`)
    try {
      this.cache = JSON.parse(
        fs.readFileSync(cacheJSON, { encoding: 'utf-8' }),
        (key, value) => {
          return key === '$$hashKey' ? undefined : value
        }
      )
    } catch (e) {
      console.error(e)
      console.warn('Resetting the cache configuration..')
      this._write('[]')
    }
  }
}

CacheService.$inject = []

export default CacheService
