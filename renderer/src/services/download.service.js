import { EventEmitter } from 'events'
import fs from 'fs'
import request from 'request'
import progress from 'request-progress'
import { join as pathJoin } from 'path'
import { unescape } from 'querystring'
import scraper from 'anime-scrape'

const bytesToMb = value => (value / (1 << 20)).toFixed(2)

class DownloadService extends EventEmitter {
  constructor(AnimeService, DirectoryService) {
    super()

    this._animeService = AnimeService
    this._directoryService = DirectoryService

    this.queue = []

    this.state = {
      downloading: false,
      pendingCancel: false,
      currentAnime: null,
      currentEpisode: null,
    }

    this._downloader = null
    this._stream = null

    this.on('stop', this.stop)
  }

  mapFoundAnimes() {
    const { foundAnimes } = this._animeService
    if (!foundAnimes.length) return foundAnimes
    const queueLinks = this.queue.map(({ link }) => link)
    const foundAnimesLinks = foundAnimes.map(({ link }) => link)

    // If a queueAnime entry isn't found in foundAnimes it's deleted.
    const deletedAnimes = queueLinks.filter(
      queueAnime => !foundAnimesLinks.includes(queueAnime)
    )
    if (deletedAnimes.length) {
      for (let d = 0; d < deletedAnimes.length; d++) {
        const anime = deletedAnimes[d]
        const animeIndex = this.queue.find(({ link }) => link === anime)
        this.queue.splice(animeIndex, 1)
      }
    }

    // If a foundAnime entry isn't found in queueAnimes it's added.
    const addedAnimes = foundAnimes.filter(
      ({ episodes, link }) => episodes.length && !queueLinks.includes(link)
    )
    if (addedAnimes.length) {
      for (let a = 0; a < addedAnimes.length; a++) {
        const anime = addedAnimes[a]
        this.queue.push({
          ...anime,
          ...{
            totalPercent: 0,
            totalDownloads: anime.episodes.length,
            download: { fileSize: null, currFileSize: null, currDlSpeed: 0 },
            errors: [],
            ignore: false,
          },
        })
      }
    }

    this.sort()

    return this.queue
  }

  sort() {
    this.queue = this.queue.sort((a, b) => {
      let score = 0

      if (a.ignore === b.ignore) {
        score += 0
      } else if (a.ignore) {
        score += 5
      } else if (!a.ignore) {
        score -= 5
      }

      if (a.episodes.length === b.episodes.length) {
        score += a.title.localeCompare(b.title)
      } else if (a.episodes.length < b.episodes.length) {
        score -= 1
      } else if (a.episodes.length > b.episodes.length) {
        score += 1
      }

      return score
    })

    return this.queue
  }

  ignore(anime) {
    const animeIndex = this.queue.find(
      queueAnime => queueAnime.link === anime.link
    )
    if (animeIndex) {
      animeIndex.ignore = !animeIndex.ignore
    }
    this.sort()
  }

  stop() {
    console.info('Attempting to stop')
    if (!this.state.pendingCancel && this.state.downloading) {
      this.state.pendingCancel = true
    }
  }

  _currentAnime() {
    let currentAnime = this.queue.filter(anime => !anime.ignore)

    if (currentAnime && currentAnime.length) {
      currentAnime = currentAnime[0]
    } else {
      return false
    }

    if (!currentAnime.episodes.length) {
      this.queue.splice(0, 1)
      if (this.queue.length) {
        currentAnime = this.queue[0]
      } else {
        return false
      }
    }

    return currentAnime
  }

  _currentEpisode() {
    const currentAnime = this._currentAnime()

    if (!currentAnime) {
      return false
    }

    const filteredEpisodes = currentAnime.episodes.filter(
      episode => !currentAnime.errors.includes(episode)
    )

    return filteredEpisodes[0]
  }

  async start() {
    console.info('Attempting to start..')
    if (this.state.downloading) {
      return false
    }

    const currentAnime = this._currentAnime()
    const currentEpisode = this._currentEpisode()
    Object.assign(this.state, { currentAnime, currentEpisode })

    if (!currentAnime) {
      return false
    }

    const { link } = currentAnime

    console.info(
      `Fetching the download uri for ${link} episode ${currentEpisode}..`
    )

    const uri = unescape(await scraper.getDownload(link, currentEpisode))

    if (uri === 'null') {
      this.state.downloading = true
      currentAnime.errors.push(currentEpisode)
      console.info(
        `Failed to fetch the download uri for '${link} episode ${currentEpisode}'`
      )
      return this._end()
    }

    console.info(
      `Retrieved the download uri '${window.encodeURI(
        uri
      )}' for '${link} episode ${currentEpisode}'`
    )

    const episodeDir = pathJoin(
      this._directoryService.path,
      link,
      `Episode ${currentEpisode}.mp4`
    )

    this._stream = fs.createWriteStream(episodeDir)

    try {
      this._downloader = progress(
        request({
          uri,
          method: 'GET',
          headers: {
            'User-Agent':
              'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0',
          },
        })
      )

      this.state.downloading = true

      this._downloader.pipe(this._stream)
      this._downloader.on('end', this._end)
      this._downloader.on('progress', this._progress)
      this._downloader.on('error', this._error)
    } catch (e) {
      this.state.downloading = false
    }
  }

  _error = error => {
    if (this.state.downloading) {
      this.state.downloading = false
      this._stream.end()
      const { currentAnime, currentEpisode } = this.state
      const episodeDir = pathJoin(
        this._directoryService.path,
        currentAnime.link,
        `Episode ${currentEpisode}.mp4`
      )

      currentAnime.errors.push(currentEpisode)

      if (fs.existsSync(episodeDir)) {
        fs.unlinkSync(episodeDir)
      }

      this._stream.on('finish', () => {
        this.start()
      })
    }
    this.emit('error', error)
  }

  _progress = progress => {
    const { percent, speed, size } = progress
    if (this.state.pendingCancel) {
      console.info('Attempting to stop the download process.')
      this._downloader.abort()
    }

    const { currentAnime } = this.state
    const downloadIndex =
      currentAnime.totalDownloads - currentAnime.episodes.length + percent
    const totalPercent = (downloadIndex / currentAnime.totalDownloads) * 100

    Object.assign(currentAnime, {
      totalPercent,
      download: {
        fileSize: bytesToMb(size.total),
        currFileSize: bytesToMb(size.transferred),
        currDlSpeed: bytesToMb(speed),
      },
    })
    this.emit('progress', progress)
  }

  _end = () => {
    const { currentAnime, currentEpisode } = this.state
    const episodeDir = pathJoin(
      this._directoryService.path,
      currentAnime.link,
      `Episode ${currentEpisode}.mp4`
    )

    this.state.downloading = false

    if (this.state.pendingCancel) {
      this.state.pendingCancel = false

      if (fs.existsSync(episodeDir)) {
        fs.unlinkSync(episodeDir)
      }
    } else {
      console.info(
        `Attempting to splice episode ${currentEpisode} off from ${currentAnime.title}.`
      )
      const episodeIndex = currentAnime.episodes.indexOf(currentEpisode)
      currentAnime.episodes.splice(episodeIndex, 1)
      this.start()
    }

    this.emit('end', { link: currentAnime.link, episode: currentEpisode })
  }
}

DownloadService.$inject = ['AnimeService', 'DirectoryService']

export default DownloadService
