import angular from 'angular'

import DownloadService from './download.service'
import DirectoryService from './directory.service'
import SettingsService from './settings.service'
import AnimeService from './anime.service'
import CacheService from './cache.services'

const moduleName = 'app.services'

angular
  .module(moduleName, [])
  .service('DirectoryService', DirectoryService)
  .service('DownloadService', DownloadService)
  .service('SettingsService', SettingsService)
  .service('AnimeService', AnimeService)
  .service('CacheService', CacheService)

export default moduleName
