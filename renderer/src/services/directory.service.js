import * as fs from 'fs'
import * as path from 'path'
import { promisify } from 'util'
import rimraf from 'rimraf'
import { remote } from 'electron'

const { dialog, shell, app } = remote

const rimrafPromise = promisify(rimraf)

class DirectoryService {
  constructor(SettingsService, CacheService, AnimeService, $mdDialog) {
    this._settingsService = SettingsService
    this._cacheService = CacheService
    this._animeService = AnimeService
    this._$mdDialog = $mdDialog

    this.path = this._settingsService.get('path')

    this.change = this.change.bind(this)
    this.scan = this.scan.bind(this)
  }

  open(path) {
    return shell.openItem(path)
  }

  change() {
    const directory = dialog.showOpenDialog({
      properties: ['openDirectory'],
    })
    if (!directory) {
      this._$mdDialog
        .show(
          this._$mdDialog
            .alert()
            .clickOutsideToClose()
            .title('Error')
            .textContent('Directory was not selected.')
            .ariaLabel('ErrorDialog')
            .ok('OK')
        )
        .then(() => {
          if (!this.path) app.quit()
        })
      return false
    }
    if (directory[0] === this.path) {
      this._$mdDialog.show(
        this._$mdDialog
          .alert()
          .clickOutsideToClose()
          .title('Error')
          .textContent('This is already your directory.')
          .ariaLabel('ErrorDialog')
          .ok('OK')
      )
      return false
    }
    this.path = directory.shift()
    this._settingsService.set('path', this.path)
    return true
  }

  scan() {
    if (!this.path) throw new Error('"path" is missing.')
    if (!fs.existsSync(this.path)) {
      throw new Error(`The path "${this.path}" doesn't exist.`)
    }

    const animeList = this._animeService.animeList.map(({ link }) => link)
    const foundAnimeFolders = fs
      .readdirSync(this.path)
      .filter(
        folder =>
          fs.lstatSync(path.join(this.path, folder)).isDirectory() &&
          animeList.includes(folder)
      )

    return foundAnimeFolders
  }

  async getEpisodes(anime) {
    const obtainPath = ({ link, episode }) =>
      path.join(this.path, link, `Episode ${episode}.mp4`)

    const foundEpisodes = anime.episodes.filter(e =>
      fs.existsSync(obtainPath({ link: anime.link, episode: e }))
    )

    for (let e = 0; e < foundEpisodes.length; e++) {
      const episode = foundEpisodes[e]

      const details = { link: anime.link, episode }
      const { size } = fs.statSync(obtainPath(details))

      if (Math.floor(size / 1024 / 1024) <= 3) {
        console.info(`Removing '${obtainPath(details)}' since it's corrupted.`)
        await rimrafPromise(obtainPath(details))
      }
    }

    const newEpisodes = anime.episodes.filter(
      e => !fs.existsSync(obtainPath({ link: anime.link, episode: e }))
    )

    return newEpisodes
  }
}

DirectoryService.$inject = [
  'SettingsService',
  'CacheService',
  'AnimeService',
  '$mdDialog',
]

export default DirectoryService
