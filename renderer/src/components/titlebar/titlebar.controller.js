import { remote } from 'electron'

class TitlebarController {
  constructor($rootScope, DownloadService) {
    this._$rootScope = $rootScope
    this._downloadService = DownloadService

    this._$rootScope.updates = undefined

    this._updateElement = null

    this._$rootScope.$watch('updates', value => {
      if (!Number.isNaN(value) && !this._updateElement) {
        process.nextTick(() => {
          this._updateElement = document.querySelector(
            'div.titlebar-update-wrapper > md-icon'
          )
        })
      }
    })

    this.animes = this._downloadService.mapFoundAnimes()

    this._downloadInterval = null
    this._currentDeg = 0

    this._downloadService.on('progress', () => {
      if (!this.downloadInterval) {
        this.downloadInterval = window.setInterval(() => {
          this._rotateUpdateIcon()
        }, 1000)
      }
    })

    this._downloadService.on('end', () => {
      this._$rootScope.updates = this._getUpdates()
      if (this.downloadInterval) {
        window.clearInterval(this.downloadInterval)
        this.downloadInterval = null
        this._rotateUpdateIcon(true)
      }
    })
  }

  _rotateUpdateIcon(reset = false) {
    if (reset) {
      this._currentDeg = 0
    } else {
      if (this._currentDeg === 360) {
        this._currentDeg = 0
      }
      this._currentDeg += 30
    }
    if (this._updateElement) {
      this._updateElement.style.transform = `rotate(${this._currentDeg}deg)`
    } else {
      console.error('Unable to find the update element.')
    }
  }

  _getUpdates() {
    if (!this.animes.length) return 0
    return this.animes
      .map(({ episodes }) => episodes.length)
      .reduce((a, b) => a + b)
  }

  close() {
    remote.getCurrentWindow().close()
  }

  minimize() {
    remote.getCurrentWindow().minimize()
  }

  maximize() {
    const currentWindow = remote.getCurrentWindow()
    if (currentWindow.isMaximized()) {
      currentWindow.restore()
    } else {
      currentWindow.maximize()
    }
  }
}

TitlebarController.$inject = ['$rootScope', 'DownloadService']

export default TitlebarController
