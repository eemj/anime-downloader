import controller from './titlebar.controller'
import './titlebar.styles.scss'
import template from './titlebar.template.html'

export default {
  controller,
  template,
  controllerAs: 'titlebar',
}
