import { remote } from 'electron'

const { app, shell } = remote
const homePath = app.getPath('home')

class DownloadController {
  constructor(
    $mdDialog,
    $scope,
    AnimeService,
    DirectoryService,
    SettingsService,
    DownloadService
  ) {
    'ngInject'

    this._$mdDialog = $mdDialog
    this._$scope = $scope
    this._animeService = AnimeService
    this._directoryService = DirectoryService
    this._settingsService = SettingsService
    this._downloadService = DownloadService

    this.path = this._directoryService.path

    this.displayPath = this.path.startsWith(homePath)
      ? this.path.replace(homePath, '~')
      : this.path

    this.animes = this._downloadService.mapFoundAnimes()

    this.state = this._downloadService.state
  }

  filteredEpisodes(anime) {
    return anime.episodes.filter(episode => !anime.errors.includes(episode))
  }

  cancel() {
    this._$mdDialog.hide()
  }

  clickPath() {
    shell.openItem(this.path)
  }

  delete(target) {
    if (!this.state.downloading) {
      const anime = this.animes.find(({ link }) => link === target)
      this._downloadService.ignore(anime)
    }
  }

  reloadScope() {
    if (!this._$scope.$$phase) {
      this._$scope.$apply()
    }
  }

  stop() {
    if (!this.state.pendingCancel && this.state.downloading) {
      this.state.pendingCancel = true
    }
  }

  start() {
    this._downloadService.start()

    this._downloadService.on('progress', () => {
      this.reloadScope()
    })

    this._downloadService.on('error', error => {
      console.error(`An error has occured: '${error}'`)
    })

    this._downloadService.on('end', ({ link, episode }) => {
      console.info(`${link}-episode-${episode} has finished downloading.`)
      this.reloadScope()
    })
  }
}

DownloadController.$inject = [
  '$mdDialog',
  '$scope',
  'AnimeService',
  'DirectoryService',
  'SettingsService',
  'DownloadService',
]

export default DownloadController
