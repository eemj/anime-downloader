import scraper from 'anime-scrape'
import { existsSync } from 'fs'
import { EventEmitter } from 'events'
import { cpus as numCPUs } from 'os'

const COMPLETE_STATUS = 'finished'
const CONCURRENT_WORKERS = numCPUs().length

class LoadController {
  constructor(
    $mdDialog,
    $state,
    $rootScope,
    $scope,
    DirectoryService,
    AnimeService,
    SettingsService,
    CacheService
  ) {
    this._$mdDialog = $mdDialog
    this._$state = $state
    this._$rootScope = $rootScope
    this._$scope = $scope
    this._directoryService = DirectoryService
    this._animeService = AnimeService
    this._settingsService = SettingsService
    this._cacheService = CacheService

    this.path = this._directoryService.path

    if (!this.path || !existsSync(this.path)) {
      this._directoryService.change()
    }

    this.percentage = 0
    this.details = []
    this.queue = []
    this.working = 0

    // Holding the value for the latest animes
    this.latest = []

    this.worker = new EventEmitter()
    this.worker.on('fetch', async anime => {
      console.info(`Fetching '${anime}'`)
      const details = await this.getDetails(anime)
      this.details.push(details)

      this.working--
      this.worker.emit('fetched')

      if (this.queue.length) {
        this.queueUp(this.queue.pop())
      }
    })

    this.init()
  }

  reloadScope() {
    if (!this._$scope.$$phase) {
      this._$scope.$apply()
    }
  }

  queueUp(anime) {
    if (this.working < CONCURRENT_WORKERS) {
      this.worker.emit('fetch', anime)
      this.working++
    } else {
      this.queue.push(anime)
    }
  }

  async getDetails(anime) {
    // If we find it in the caching layer, we save a few seconds from retrieving data from the API.
    if (this._cacheService.has(anime)) {
      console.info(`Found '${anime}' in the caching layer..`)
      return this._cacheService.get(anime)
    }

    const details = await scraper.getDetail(anime)

    if (details.status.toLowerCase() === COMPLETE_STATUS) {
      this._cacheService.add(anime, details)
      console.info(`Adding '${anime}' to the caching layer..`)
    } else {
      // In rare cases, the latest anime can contain more than one episode in our array
      const latest = this.latest
        .filter(
          ({ link, episode }) =>
            anime === link && !details.episodes.includes(episode)
        )
        .map(({ episode }) => episode)
      if (latest.length) {
        details.episodes.push(...latest)
      }
    }

    return details
  }

  async init() {
    this._$rootScope.updates = undefined

    this.percentage = 0
    const startTime = Date.now()

    if (!this._animeService.animeList.length) {
      this._animeService.animeList = await scraper.getList()
    }

    // adding our anime to the queue
    let foundAnimes = await this._directoryService.scan()

    if (foundAnimes.length) {
      console.info(`Running with ${CONCURRENT_WORKERS} concurrent workers.`)

      this.latest = (await scraper.getLatest()).filter(({ link }) =>
        foundAnimes.includes(link)
      )

      for (const anime of foundAnimes) {
        this.queueUp(anime)
      }

      let increment = (1 / foundAnimes.length) * 100
      this.worker.on('fetched', () => {
        this.percentage += increment
        this.reloadScope()
      })

      // blocking the process till every anime is fetched
      await new Promise(async resolve => {
        while (this.details.length < foundAnimes.length) {
          // eslint-disable-next-line
          await new Promise(r => setTimeout(r, 300)) // ensure that we won't call the parent resolve
        }
        resolve()
      })

      // replace the found animes with the details
      foundAnimes = this.details

      // now that we have all the anime details, let's check what animes the user owns
      increment = (100 - this.percentage) / foundAnimes.length
      for (const anime of foundAnimes) {
        anime.episodes = await this._directoryService.getEpisodes(anime)
        this.percentage += increment
        this.reloadScope()
      }

      console.info(
        `Took around ${(Date.now() - startTime) / 1000}s to finish..`
      )

      this._animeService.foundAnimes = foundAnimes
    }

    this._$state.go('home')
  }
}

LoadController.$inject = [
  '$mdDialog',
  '$state',
  '$rootScope',
  '$scope',
  'DirectoryService',
  'AnimeService',
  'SettingsService',
  'CacheService',
]

export default LoadController
