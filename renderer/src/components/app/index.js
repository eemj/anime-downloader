import template from './app.template.html'
import controller from './app.controller'
import './app.styles.scss'

const AppComponent = { template, controller }

export default AppComponent
