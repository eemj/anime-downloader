pipeline {
    agent {
        docker {
            image 'eemj/electron-builder:3'
            args "\
                --entrypoint='' \
                --user root \
                -v ${GLOBAL_CACHE}/electron_cache:/root/.cache/electron \
                -v ${GLOBAL_CACHE}/electron_builder_cache:/root/.cache/electron-builder \
                -v ${GLOBAL_CACHE}/yarn_cache:/root/.cache/yarn"
        }
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: "10"))
    }

    stages {
        stage("Version") {
            steps {
                script {
                    packageVersion = "#${BUILD_NUMBER} " + sh(
                        script:'node -e \'console.log(`v${require("./package.json").version}`)\'',
                        returnStdout: true
                    ).trim()
                    currentBuild.displayName = packageVersion
                }
            }
        }

        stage("Dependencies") {
            steps {
                script {
                    try {
                        sh '''
                            mkdir -p ~/.cache/yarn && yarn config set cache-folder ~/.cache/yarn
                            mkdir ~/.yarn && yarn config set prefix ~/.yarn
                            yarn install
                            zip node_modules.zip node_modules/ -1 -r -q
                        '''
                        archiveArtifacts "node_modules.zip"
                    } catch (err) {
                        deleteDir()
                    }
                }
            }
        }

        stage("Build") {
            parallel {
                stage("Webpack") {
                    steps {
                        sh "yarn run build"
                    }
                }

                stage("Linting") {
                    steps {
                        sh "yarn run lint -f checkstyle -o eslint.xml || echo 'Linting failed, proceeding on with the build.'"
                        step([$class: "hudson.plugins.checkstyle.CheckStylePublisher", pattern: "**/eslint.xml"])
                    }
                }
            }
        }

        stage("Release") {
            steps {
                sh '''
                    yarn run release
                    chmod +x scripts/checksums
                    ./scripts/checksums > checksums.txt
                '''
                archiveArtifacts "checksums.txt, releases/**"
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
