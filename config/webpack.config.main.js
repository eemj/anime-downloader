require('dotenv-extended').load({ errorOnMisisng: true })

const { join: pathJoin } = require('path')
const defaults = require('./defaults')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  mode: 'production',
  context: defaults.mainSrc,
  entry: [require.resolve('babel-polyfill'), './index.js'],
  target: 'electron-main',
  output: {
    filename: 'index.js',
    path: defaults.mainDist,
    pathinfo: true,
  },
  node: {
    __dirname: false,
    __filename: false,
  },
  optimization: {
    minimize: true,
    minimizer: [
      new UglifyJsPlugin({
        parallel: true,
        uglifyOptions: {
          output: { comments: false },
        },
      }),
    ],
  },
  plugins: [],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: require.resolve('cache-loader'),
            options: {
              cacheDirectory: pathJoin(
                defaults.nodeModules,
                './.cache/babel-loader'
              ),
              cacheIdentifier: '6265b22b66502d70d5f0',
            },
          },
          {
            loader: require.resolve('babel-loader'),
            options: {
              compact: true,
              babelrc: false,
              presets: [
                [
                  require.resolve('babel-preset-env'),
                  {
                    targets: {
                      uglify: true,
                    },
                    useBuiltIns: false,
                  },
                ],
                require.resolve('babel-preset-stage-2'),
              ],
              plugins: [
                [
                  require.resolve('babel-plugin-transform-runtime'),
                  {
                    helpers: false,
                    polyfill: false,
                    regenerator: true,
                  },
                ],
              ],
            },
          },
        ],
      },
    ],
  },
  stats: 'errors-only',
}
