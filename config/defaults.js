const path = require('path')
const resolve = args => path.resolve(__dirname, '../', args)

module.exports = {
  // Main Process
  logoPng: resolve('../resources/icon.png'),
  notifPng: resolve('../resources/icon_notification.png'),
  mainSrc: resolve('main/src'),
  mainDist: resolve('main/dist'),
  indexHtml: resolve('../renderer/dist/index.html'),

  // Renderer Process
  devPort: 4200,
  devHostname: 'localhost',
  rendererSrc: resolve('renderer/src'),
  rendererDist: resolve('renderer/dist'),
  releases: resolve('releases'),
  template: resolve('renderer/src/assets/index.ejs'),
  nodeModules: resolve('node_modules'),
}
