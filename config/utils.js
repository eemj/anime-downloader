const { NODE_ENV } = process.env

exports.NODE_ENV = NODE_ENV
exports.isDevelopment = () => NODE_ENV === 'development'
exports.isProduction = (isTrue, isFalse) =>
  NODE_ENV === 'production' ? isTrue : isFalse
