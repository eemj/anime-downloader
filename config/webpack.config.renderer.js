require('dotenv-extended').load({ errorOnMisisng: true })

const { join: pathJoin } = require('path')
const { spawn } = require('child_process')
const webpack = require('webpack')
const chalk = require('chalk')
const defaults = require('./defaults')

// Webpack Plugins
const HTMLPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const { DISABLE_ELECTRON } = process.env
const { isProduction, isDevelopment, NODE_ENV } = require('./utils')

console.info(`\n\rRunning in ${chalk.bold.blue(NODE_ENV.toLowerCase())} mode.`)

const publicPath = `http://${defaults.devHostname}:${defaults.devPort}/`
const defaultEntry = [require.resolve('babel-polyfill'), './app.js']

// Getting who the main executor is, either `webpack` or `webpack-dev-server`.
let parentExec = process.argv[1].split(
  require('os').platform() === 'win32' ? '\\' : '/'
)
parentExec = parentExec[parentExec.length - 1]
if (parentExec.indexOf('.')) {
  parentExec = parentExec.split('.')[0]
}

const startScript = (script = 'preview') => {
  spawn('npm', [...`run ${script}`.split(' ')], {
    shell: true,
    stdio: 'inherit',
  })
    .on('close', () => {
      process.exit(0)
    })
    .on('error', spawnError => {
      throw spawnError
    })
}

module.exports = {
  mode: NODE_ENV,
  bail: true,
  target: 'electron-renderer',
  devtool: isProduction('cheap-module-source-map', 'inline-source-map'),
  context: defaults.rendererSrc,
  entry: isProduction(defaultEntry, [
    'webpack/hot/dev-server',
    `webpack-dev-server/client?${publicPath}`,
    ...defaultEntry,
  ]),
  output: {
    filename: `${isProduction('[hash:16]', '[name]')}.js`,
    path: defaults.rendererDist,
    publicPath: isProduction('./', publicPath),
    pathinfo: false,
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
          enforce: true,
        },
      },
    },
    minimize: !isDevelopment(),
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
        uglifyOptions: {
          output: { comments: false },
        },
      }),
      new OptimizeCSSAssetsWebpackPlugin({}),
    ],
  },
  devServer: {
    publicPath,
    port: defaults.devPort,
    contentBase: defaults.rendererDist,
    stats: 'errors-only',
    after: function() {
      startScript()
    },
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: require.resolve('cache-loader'),
            options: {
              cacheDirectory: pathJoin(
                defaults.nodeModules,
                './.cache/babel-loader'
              ),
              cacheIdentifier: '6265b22b66502d70d5f0',
            },
          },
          {
            loader: require.resolve('babel-loader'),
            options: {
              compact: true,
              babelrc: false,
              presets: [
                [
                  require.resolve('babel-preset-env'),
                  {
                    targets: {
                      ie: 9,
                      uglify: true,
                    },
                    useBuiltIns: false,
                  },
                ],
                require.resolve('babel-preset-stage-2'),
              ],
              plugins: [
                [
                  require.resolve('babel-plugin-transform-runtime'),
                  {
                    helpers: false,
                    polyfill: false,
                    regenerator: true,
                  },
                ],
              ],
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: require.resolve('raw-loader'),
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        loader: 'url-loader',
        options: {
          limit: 4096,
          fallback: {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[hash:8].[ext]',
            },
          },
        },
        include: pathJoin(defaults.rendererSrc, './assets/fonts'),
      },
      {
        test: /\.(css|scss)$/,
        use: [
          isProduction(MiniCssExtractPlugin.loader, {
            loader: require.resolve('style-loader'),
            options: {
              sourceMap: false,
            },
          }),
          {
            loader: require.resolve('css-loader'),
            options: {
              importLoaders: 1,
              minimize: isProduction(
                { discardComments: { removeAll: true } },
                false
              ),
            },
          },
          {
            loader: require.resolve('postcss-loader'),
            options: {
              plugins: [
                require('autoprefixer')({
                  browsers: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9',
                  ],
                  flexbox: 'no-2009',
                }),
              ],
            },
          },
          require.resolve('sass-loader'),
        ],
      },
    ],
  },
  stats: 'errors-only',
  plugins: [
    new HTMLPlugin({
      template: defaults.template,
      inject: true,
      minify: isProduction(false, {
        removeComments: true,
        collapseWhitespace: true,
        minifyURLs: true,
        minifyJS: true,
        minifyCSS: true,
        keepClosingSlash: true,
        removeEmptyAttributes: true,
      }),
      csp: !isDevelopment(),
    }),
    new MiniCssExtractPlugin({
      filename: '[contenthash].css',
      chunkFilename: '[id].[hash].css',
      allChunks: true,
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.LoaderOptionsPlugin({ debug: true }),
    new webpack.ProgressPlugin(percentage => {
      if (percentage === 1 && parentExec === 'webpack' && !DISABLE_ELECTRON) {
        console.debug('Starting webpack-dev-server..')
        startScript('webpack:server')
      }
    }),
  ],
  resolve: {
    symlinks: false,
  },
}
